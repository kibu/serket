import cv2
import malac_detector
import bgsubstract
import numpy as np
import socketio as io
import time
from misc.background_substraction import ValyuRemover


def isAvailable(blob, type):
    isAvailable = False

    eatingBlobHeads = [x['head'] for x in eatingBlobs]
    drinkingBlobHeads = [x['head'] for x in drinkingBlobs]
    aggressiveBlobHeads = [x['head'] for x in aggressiveBlobs]
    friendlyBlobHeads = [x['head'] for x in friendlyBlobs]

    if type == 'eating':
        if blob['head'] not in eatingBlobHeads:
            isAvailable = True

    if type == 'drinking':
        if blob['head'] not in drinkingBlobHeads:
            isAvailable = True

    if type == 'friendly':
        if blob['head'] not in friendlyBlobHeads and blob['head'] not in aggressiveBlobHeads and blob['head'] not in eatingBlobHeads and blob['head'] not in drinkingBlobHeads:
            isAvailable = True

    if type == 'aggressive':
        if blob['head'] not in aggressiveBlobHeads and blob['head'] not in eatingBlobHeads and blob['head'] not in drinkingBlobHeads:
            isAvailable = True

    return isAvailable


def isAggressive(a, b):
    isAggressive = False

    if isAvailable(a, 'aggressive') and isAvailable(b, 'aggressive'):
        backDist = cv2.norm(np.int16(a['butt']) - np.int16(b['butt']), cv2.NORM_L1)
        isAggressive = backDist > 210

    return isAggressive


def isFriendly(a, b, maxDist):
    isFriendly = False
    if isAvailable(a, 'friendly') and isAvailable(b, 'friendly'):
        backDist = cv2.norm(np.int16(a['butt']) - np.int16(b['butt']), cv2.NORM_L1)
        isFriendly = backDist < maxDist
    return isFriendly


def isEating(blob):
    isEating = False
    if isAvailable(blob, 'eating'):
        isEating = blob['head'][0] < 80 and blob['head'][1] > 130
    return isEating


def isDrinking(blob):
    isDrinking = False
    if isAvailable(blob, 'drinking'):
        isDrinking = 130 < blob['head'][0] < 420 and blob['head'][1] < 100
    return isDrinking


def addFriendly(a, b):
    min3 = False
    for blob in currentBlobs:
        if a != blob and b != blob:
            if isFriendly(a, blob, 200) or isFriendly(b, blob, 200):
                friendlyBlobs.append(blob)
                min3 = True

    if min3:
        friendlyBlobs.append(a)
        friendlyBlobs.append(b)


def addAggressive(a, b):
    aggressiveBlobs.append(a)
    aggressiveBlobs.append(b)
    for blob in currentBlobs:
        frontDist1 = cv2.norm(np.int16(a['head']) - np.int16(blob['head']), cv2.NORM_L1)
        frontDist2 = cv2.norm(np.int16(b['head']) - np.int16(blob['head']), cv2.NORM_L1)
        if (frontDist1 < 75 or frontDist2 < 75) and isAvailable(blob, 'aggressive'):
            if blob not in aggressiveBlobs:
                aggressiveBlobs.append(blob)


def addEating(blob):
    eatingBlobs.append(blob)


def addDrinking(blob):
    drinkingBlobs.append(blob)


def drawAggressive():
    # print(len(aggressiveBlobs))
    for blob in aggressiveBlobs:
        # print(blob['box'])
        cv2.drawContours(roi, [blob['box']], 0, (0, 0, 255), 2)


def drawFriendly():
    # print(len(friendlyBlobs))
    for blob in friendlyBlobs:
        # print(blob['box'])
        cv2.drawContours(roi, [blob['box']], 0, (0, 255, 0), 2)


def drawEating():
    for blob in eatingBlobs:
        # print(blob['box'])
        cv2.drawContours(roi, [blob['box']], 0, (0, 0, 0), 2)


def drawDrinking():
    for blob in drinkingBlobs:
        # print(blob['box'])
        cv2.drawContours(roi, [blob['box']], 0, (255, 0, 0), 2)


def checkPosition(old_blobs, current_blobs):
    movingCount = 0
    hasMoved = False

    if len(old_blobs) == len(current_blobs):
        for b in old_blobs:
            shortestDist = 1000
            for bl in current_blobs:
                dist = cv2.norm(np.int16(b['head']) - np.int16(bl['head']), cv2.NORM_L1)
                if dist < shortestDist:
                    shortestDist = dist
            # print(shortestDist)
            if shortestDist > 100:
                movingCount += 1
                hasMoved = True

    # elif len(oldBlobs) > len(currentBlobs):
    #     diff = len(oldBlobs) - len(currentBlobs)
    #     hasMoved = True
    #     movingCount += diff
    # elif len(oldBlobs) < len(currentBlobs):
    #     diff = len(currentBlobs) - len(oldBlobs)
    #     hasMoved = True
    #     movingCount += diff

    if hasMoved:
        sendData(5, movingCount)
        #print("blobs moved: {}".format(movingCount))
        old_blobs.clear()
        old_blobs = current_blobs.copy()
        sendData(5, 0)

    return old_blobs


def sendData(type, qty):
    timestamp = int(time.time())
    client.emit("message", {"timestamp": timestamp, "type": type, "qty": qty})
    print("message", {"timestamp": timestamp, "type": type, "qty": qty})


if __name__ == "__main__":
    client = io.Client()
    client.connect("http://192.168.10.114:8008")
    sendData(0, 12)  # send total malac count once

    cap = cv2.VideoCapture(2)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    backSub = bgsubstract.init(100, 30)

    vr = ValyuRemover()
    vr.init(median_src="misc/median_frame.jpg")
    vr.create_trackbars()

    currentBlobs = []
    oldBlobs = []
    friendlyBlobs = []
    aggressiveBlobs = []
    eatingBlobs = []
    drinkingBlobs = []

    aggressiveCount = 0
    friendlyCount = 0
    eatingCount = 0
    drinkingCount = 0


    while True:
        aggressiveBlobs.clear()
        friendlyBlobs.clear()
        eatingBlobs.clear()
        drinkingBlobs.clear()

        img = cap.read()[1]
        roi = img[350:990, 498:1410]
        gray_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
        gray_roi_without_valyuz = vr.remove_valyuz(gray_roi)
        malacok = malac_detector.get_malacok(gray_roi_without_valyuz)

        for malac in malacok:
            cv2.circle(roi, malac['head'], 50, (0, 0, 255), 1)
            cv2.circle(roi, malac['butt'], 100, (255, 0, 0), 1)
            # cv2.drawContours(roi, [malac['box']], 0, (0, 0, 255), 2)

        currentBlobs = malacok.copy()
        if len(oldBlobs) == 0:
            oldBlobs = malacok.copy()

        if bgsubstract.checkMovement(roi, backSub) < 2:
            oldBlobs = checkPosition(oldBlobs, currentBlobs)

        for blob in currentBlobs:
            if isEating(blob):
                addEating(blob)
            if isDrinking(blob):
                addDrinking(blob)

        for i, b in enumerate(currentBlobs):
            for j, bl in enumerate(currentBlobs[i + 1:]):
                distance = cv2.norm(np.int16(b['head']) - np.int16(bl['head']), cv2.NORM_L1)
                if distance < 75:
                    if isAggressive(b, bl):
                        addAggressive(b, bl)

        for i, b in enumerate(currentBlobs):
            for j, bl in enumerate(currentBlobs[i + 1:]):
                distance = cv2.norm(np.int16(b['head']) - np.int16(bl['head']), cv2.NORM_L1)
                if distance < 100:
                    if isFriendly(b, bl, 200):
                        addFriendly(b, bl)
                if isFriendly(b, bl, 50):
                    addFriendly(b, bl)

        drawEating()
        drawDrinking()
        drawAggressive()
        drawFriendly()

        if eatingCount != len(eatingBlobs):
            sendData(1, len(eatingBlobs))

        if drinkingCount != len(drinkingBlobs):
            sendData(2, len(drinkingBlobs))

        if aggressiveCount != len(aggressiveBlobs):
            sendData(3, len(aggressiveBlobs))

        if friendlyCount != len(friendlyBlobs):
            sendData(4, len(friendlyBlobs))

        aggressiveCount = len(aggressiveBlobs)
        friendlyCount = len(friendlyBlobs)
        eatingCount = len(eatingBlobs)
        drinkingCount = len(drinkingBlobs)

        cv2.imshow('extreme points', roi)
        cv2.waitKey(1)
