import cv2
import malac_detector
from misc.background_substraction import ValyuRemover

cap = cv2.VideoCapture(1)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

vr = ValyuRemover()
vr.init(median_src="misc/median_frame.jpg")
vr.create_trackbars()


while True:
    img = cap.read()[1]
    roi = img[350:990, 498:1410]
    gray_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
    gray_roi_without_valyuz = vr.remove_valyuz(gray_roi)
    malacok = malac_detector.get_malacok(gray_roi_without_valyuz)

    for malac in malacok:
        cv2.circle(roi, malac['head'], 9, (0, 0, 255), -1)
        cv2.circle(roi, malac['butt'], 9, (255, 0, 0), -1)

        cv2.drawContours(roi, [malac['box']], 0, (0, 0, 255), 2)

    cv2.imshow('extreme points', roi)
    cv2.waitKey(1)
