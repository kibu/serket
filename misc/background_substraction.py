import cv2
import numpy as np


class ValyuRemover:

    median = None

    def init(cls, cap=None, num_frame=25, roi=None, median_src=None):

        if median_src is not None:
            cls.median = cv2.imread(median_src, cv2.IMREAD_GRAYSCALE)
            return

        frames = []
        for i in range(num_frame):
            img = cap.read()[1]
            frames.append(img)
            print('{}%'.format(i / num_frame * 100))

        # Calculate the median along the time axis
        medianFrame = np.median(frames, axis=0).astype(dtype=np.uint8)
        # Convert background to grayscale
        grayMedianFrame = cv2.cvtColor(medianFrame, cv2.COLOR_BGR2GRAY)

        if roi is not None:
            grayMedianFrame = grayMedianFrame[roi['x1']:roi['x2'], roi['y1']:roi['y2']]

        cv2.imwrite("misc/median_frame.jpg", grayMedianFrame)
        print('Median frame is saved as misc/median_frame.jpg')

        cv2.imshow("median", grayMedianFrame)
        cv2.imshow('img', img)
        cv2.moveWindow('img', 400, 400)
        cv2.waitKey(0)

        cls.median = grayMedianFrame

    def remove_valyuz(cls, img):
        if cls.median is None:
            print("Use ValyuRemover.init() first!!!")
        substracted_img = cls.__substract_median(img, cls.median)
        mask = cls.__clean(substracted_img)

        try:
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        except cv2.error:
            gray = img

        image_without_valyuz = cv2.bitwise_and(gray, mask)

        cv2.imshow("masked", image_without_valyuz)

        return image_without_valyuz

    def __substract_based_on_img(cls):
        bg = cv2.imread("/home/balint/Projects/Kibu/malacka/imgs/empty.jpg", cv2.IMREAD_GRAYSCALE)
        img = cv2.imread("/home/balint/Projects/Kibu/malacka/imgs/full.jpg", cv2.IMREAD_GRAYSCALE)

        bg_blured = cv2.GaussianBlur(bg,(5,5),0)
        img_blured = cv2.GaussianBlur(img,(5,5),0)

        diff = cv2.absdiff(img_blured, bg_blured)
        _, diff_th = cv2.threshold(diff, 60, 255, cv2.THRESH_BINARY)

        cv2.imshow('Frame', diff)
        cv2.moveWindow("Frame", 100, 60)
        cv2.imshow('FG Mask', diff_th)
        cv2.moveWindow("FG Mask", 40, 40)
        cv2.waitKey(0)

    def __substract_median(cls, img, median):
        # Calculate absolute difference of current frame and
        # the median frame
        try:
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        except cv2.error:
            gray = img

        dframe = cv2.absdiff(gray, median)
        # Treshold to binarize
        th = cls.get_trackbar_pos()[0]
        th, dframe = cv2.threshold(dframe, th, 255, cv2.THRESH_BINARY)
        # Display image
        cv2.imshow('frame', dframe)

        return dframe

    def __clean(cls, substracted_img):
        # noise removal
        kernel = np.ones((3, 3), np.uint8)
        open, close = cls.get_trackbar_pos()[1:]
        opening = cv2.morphologyEx(substracted_img, cv2.MORPH_OPEN, kernel, iterations=open)
        closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel, iterations=close)
        cv2.imshow('clean', closing)
        return closing

    def create_trackbars(cls):

        def nothing(x):
            pass

        cv2.namedWindow("settings")
        cv2.moveWindow('settings', 100, 900)

        # create trackbars for color change
        cv2.createTrackbar("thresh", 'settings', 86, 255, nothing)
        cv2.createTrackbar("open", 'settings', 1, 10, nothing)
        cv2.createTrackbar("close", 'settings', 7, 10, nothing)

    def get_trackbar_pos(cls):
        # get current positions of four trackbars
        th = cv2.getTrackbarPos('thresh', 'settings')
        open = cv2.getTrackbarPos('open', 'settings')
        close = cv2.getTrackbarPos('close', 'settings')

        return th, open, close


if __name__ == '__main__':
    cap = cv2.VideoCapture(1)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

    vr = ValyuRemover()

    vr.create_trackbars()
    vr.init(median_src="median_frame.jpg")

    while True:
        img = cap.read()[1]
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img_roi = gray[350:990, 498:1410]
        vr.remove_valyuz(img_roi)
        cv2.waitKey(1)

    # substract_based_on_img()