import cv2
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib import colors


def main():
    img = cv2.imread("imgs/my_photo-3.jpg")
    roi = img[215:954, 458:1433]

    while True:
        min, max = get_trackbar_pos()
        canny = cv2.Canny(roi, min, max)

        cv2.imshow('edge', canny)
        cv2.waitKey(1)

def get_trackbar_pos():
    # get current positions of four trackbars
    canny_min = cv2.getTrackbarPos('Canny min', 'settings')
    canny_max = cv2.getTrackbarPos('Canny max', 'settings')

    return canny_min, canny_max


def create_trackbars():

    def nothing(x):
        pass

    cv2.namedWindow("settings")

    # create trackbars for color change
    cv2.createTrackbar('Canny min', 'settings', 100, 255, nothing)
    cv2.createTrackbar('Canny max', 'settings', 200, 255, nothing)


if __name__ == "__main__":
    create_trackbars()
    main()
