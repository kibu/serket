import cv2
import numpy as np

def segmentation(img):

    # th = get_trackbar_pos()

    blur = cv2.blur(img,(5,5))

    try:
        gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
    except cv2.error:
        gray = img

    ret, thresh = cv2.threshold(gray, 66, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # noise removal
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)

    # Finding sure foreground area
    dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
    normalized = dist_transform.copy()
    cv2.normalize(dist_transform, normalized, 0, 1.0, cv2.NORM_MINMAX)

    # Dilate a bit the dist image
    _, dist = cv2.threshold(normalized, 0.4, 1.0, cv2.THRESH_BINARY)
    kernel1 = np.ones((3, 3), dtype=np.uint8)
    dist = cv2.dilate(dist, kernel1)

    # Finding unknown region
    sure_fg = np.uint8(dist*255)

    # cv2.imshow('result', img)
    # cv2.moveWindow('result', 40, 30)
    # cv2.imshow('original', sure_fg)
    # cv2.moveWindow('original', 40+img.shape[1], 30)

    return sure_fg


def create_trackbars():

    def nothing(x):
        pass

    cv2.namedWindow("settings")
    cv2.moveWindow('settings', 100, 900)

    # create trackbars for color change
    cv2.createTrackbar('1st thresh', 'settings', 1, 200, nothing)


def get_trackbar_pos():
    # get current positions of four trackbars
    canny_min = cv2.getTrackbarPos('1st thresh', 'settings')

    return canny_min


if __name__ == "__main__":
    cap = cv2.VideoCapture(2)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

    while True:
        img = cap.read()[1]
        roi = img[350:990, 498:1410]
        segmentation(roi)
        cv2.waitKey(1)