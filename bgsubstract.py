import cv2
import numpy as np


def init(th, hist):
    backSub = cv2.createBackgroundSubtractorMOG2(history=hist, varThreshold=th)
    return backSub


def checkMovement(img, backSub):
    fgMask = backSub.apply(img)
    avg = np.average(fgMask)
    return avg


def on_trackbar(x):
    global changed
    changed = True


if __name__ == "__main__":
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    backSub = init(37, 627)
    changed = False
    cv2.namedWindow("threshold")
    cv2.createTrackbar("threshold", "threshold", 37, 255, on_trackbar)
    cv2.createTrackbar("history", "threshold", 627, 2000, on_trackbar)


    while True:
        img = cap.read()[1]
        roi = img[200:900, 468:1440]
        gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)

        if changed:
            changed = False
            backSub = init(cv2.getTrackbarPos("threshold", "threshold"), cv2.getTrackbarPos("history", "threshold"))
            print("CHANGED")
        fgMask = backSub.apply(roi)
        cv2.imshow('Frame', roi)
        cv2.imshow('FG Mask', fgMask)
        print(np.average(fgMask))
        cv2.waitKey(1)