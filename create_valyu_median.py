import cv2
from misc.background_substraction import ValyuRemover

# RUN IT WITH MALAC LESS BARN!!!
# median is saved to misc/median_frame.jpg

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
crop = {'x1': 200, 'x2': 900, 'y1': 468, 'y2': 1440}

vr = ValyuRemover()
vr.init(cap=cap, num_frame=100, roi=crop)
