import cv2
import malac_detector
import bgsubstract
import numpy as np
import socketio as io
import time
import threading
from misc.background_substraction import ValyuRemover


def isAvailable(blob, activity):
    isAvailable = False

    eatingBlobHeads = [x['head'] for x in eatingBlobs]
    drinkingBlobHeads = [x['head'] for x in drinkingBlobs]
    aggressiveBlobHeads = [x['head'] for x in aggressiveBlobs]
    friendlyBlobHeads = [x['head'] for x in friendlyBlobs]

    if activity == 'eating':
        if blob['head'] not in eatingBlobHeads:
            isAvailable = True

    if activity == 'drinking':
        if blob['head'] not in drinkingBlobHeads:
            isAvailable = True

    if activity == 'friendly':
        if blob['head'] not in friendlyBlobHeads and blob['head'] not in aggressiveBlobHeads and blob['head'] not in eatingBlobHeads and blob['head'] not in drinkingBlobHeads:
            isAvailable = True

    if activity == 'aggressive':
        if blob['head'] not in aggressiveBlobHeads and blob['head'] not in eatingBlobHeads and blob['head'] not in drinkingBlobHeads:
            isAvailable = True

    return isAvailable


def isAggressive(a, b):
    isAggressive = False

    if isAvailable(a, 'aggressive') and isAvailable(b, 'aggressive'):
        backDist = cv2.norm(np.int16(a['butt']) - np.int16(b['butt']), cv2.NORM_L1)
        isAggressive = backDist > 250

    return isAggressive


def isFriendly(a, b, maxDist):
    isFriendly = False
    if isAvailable(a, 'friendly') and isAvailable(b, 'friendly'):
        backDist = cv2.norm(np.int16(a['butt']) - np.int16(b['butt']), cv2.NORM_L1)
        isFriendly = backDist < maxDist
    return isFriendly


def isEating(blob):
    isEating = False
    if isAvailable(blob, 'eating'):
        isEating = blob['head'][0] < 80 and blob['head'][1] > 110
    return isEating


def isDrinking(blob):
    isDrinking = False
    if isAvailable(blob, 'drinking'):
        isDrinking = 330 < blob['head'][0] < 640 and blob['head'][1] > 580
    return isDrinking


def addAggressive(a, b):
    aggressiveBlobs.append(a)
    aggressiveBlobs.append(b)
    for blob in currentBlobs:
        frontDist1 = cv2.norm(np.int16(a['head']) - np.int16(blob['head']), cv2.NORM_L1)
        frontDist2 = cv2.norm(np.int16(b['head']) - np.int16(blob['head']), cv2.NORM_L1)
        if (frontDist1 < 75 or frontDist2 < 75) and isAvailable(blob, 'aggressive'):
            if blob not in aggressiveBlobs:
                aggressiveBlobs.append(blob)


def addEating(blob):
    eatingBlobs.append(blob)


def addDrinking(blob):
    drinkingBlobs.append(blob)


def drawAggressive(aggressive_blobs):
    # print(len(aggressiveBlobs))
    for blob in aggressive_blobs:
        # print(blob['box'])
        cv2.drawContours(roi, [blob['box']], 0, (0, 0, 255), 2)


def drawFriendly(friendly_blobs):
    # print(len(friendlyBlobs))
    for blob in friendly_blobs:
        # print(blob['box'])
        cv2.drawContours(roi, [blob['box']], 0, (0, 255, 0), 2)


def drawEating(eating_blobs):
    for blob in eating_blobs:
        # print(blob['box'])
        cv2.drawContours(roi, [blob['box']], 0, (0, 0, 0), 2)


def drawDrinking(drinking_blobs):
    for blob in drinking_blobs:
        # print(blob['box'])
        cv2.drawContours(roi, [blob['box']], 0, (255, 0, 0), 2)


def checkPosition(old_blobs, current_blobs):
    movingCount = 0
    hasMoved = False
    global movement_time

    if len(old_blobs) == len(current_blobs):
        for b in old_blobs:
            shortestDist = 1000
            for bl in current_blobs:
                dist = cv2.norm(np.int16(b['head']) - np.int16(bl['head']), cv2.NORM_L1)
                if dist < shortestDist:
                    shortestDist = dist
            # print(shortestDist)
            if shortestDist > 10:
                movingCount += 1
                hasMoved = True

    # elif len(old_blobs) > len(current_blobs):
    #     diff = len(old_blobs) - len(current_blobs)
    #     hasMoved = True
    #     movingCount += diff
    # elif len(old_blobs) < len(current_blobs):
    #     diff = len(current_blobs) - len(old_blobs)
    #     hasMoved = True
    #     movingCount += diff

    if hasMoved:
        sendData(5, movingCount)
        movingCount = 0
        #sendData(0, len(current_blobs))
        # print("blobs moved: {}".format(movingCount))
        old_blobs.clear()
        old_blobs = current_blobs.copy()

        # timer = threading.Timer(3.0, reset_moving)
        # timer.start()

        movement_time = time.time()


    return old_blobs





def get_friendly_blobs(blobs):
    friendships = [x['head'] for b in blobs for x in b]
    friendly_blobs = []

    for blob_pair in blobs:
        if friendships.count(blob_pair[0]['head']) > 1 or friendships.count(blob_pair[1]['head']) > 1:
            for blob in blob_pair:
                friendly_heads = [x['head'] for x in friendly_blobs]
                if blob['head'] not in friendly_heads:
                    friendly_blobs.append(blob)

    return friendly_blobs


def sendData(activity, qty, clientVal = None):
    timestamp = time.time()
    if clientVal is None:
        clientVal = client
    clientVal.emit("message", {"timestamp": timestamp, "type": activity, "qty": qty})
    print("message", {"timestamp": timestamp, "type": activity, "qty": qty})


def checkState(aggressive_blobs, friendly_blobs, eating_blobs, drinking_blobs, current_blobs):
    aggressiveCount = len(aggressive_blobs)
    friendlyCount = len(friendly_blobs)
    eatingCount = len(eating_blobs)
    drinkingCount = len(drinking_blobs)

    aggressive_blobs.clear()
    friendly_blobs.clear()
    eating_blobs.clear()
    drinking_blobs.clear()
    tempFriendlyBlobs = []

    for blob in current_blobs:
        if isEating(blob):
            addEating(blob)
        if isDrinking(blob):
            addDrinking(blob)

    for i, b in enumerate(current_blobs):
        for j, bl in enumerate(current_blobs[i + 1:]):
            distance = cv2.norm(np.int16(b['head']) - np.int16(bl['head']), cv2.NORM_L1)
            if distance < 75:
                if isAggressive(b, bl):
                    addAggressive(b, bl)

    for i, b in enumerate(current_blobs):
        for j, bl in enumerate(current_blobs[i + 1:]):
            distance = cv2.norm(np.int16(b['head']) - np.int16(bl['head']), cv2.NORM_L1)
            if distance < 75:
                if isFriendly(b, bl, 250):
                    tempFriendlyBlobs.append([b, bl])
            else:
                if isFriendly(b, bl, 100):
                    tempFriendlyBlobs.append([b, bl])

    friendly_blobs = get_friendly_blobs(tempFriendlyBlobs)

    if eatingCount != len(eating_blobs):
        sendData(1, len(eating_blobs))

    if drinkingCount != len(drinking_blobs):
        sendData(2, len(drinking_blobs))

    if aggressiveCount != len(aggressive_blobs):
        sendData(3, len(aggressive_blobs))

    if friendlyCount != len(friendly_blobs):
        sendData(4, len(friendly_blobs))

    return aggressive_blobs, friendly_blobs, eating_blobs, drinking_blobs


if __name__ == "__main__":
    client = io.Client()
    client.connect("http://localhost:8008")
    sendData(0, 12)  # send total malac count once
    for x in range(1, 6):
        sendData(x, 0)  # send starting data to reset the infographic

    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    backSub = bgsubstract.init(th=37, hist=273)

    vr = ValyuRemover()
    vr.init(median_src="misc/median_frame.jpg")
    vr.create_trackbars()

    currentBlobs = []
    oldBlobs = []
    friendlyBlobs = []
    aggressiveBlobs = []
    eatingBlobs = []
    drinkingBlobs = []

    movementOccurred = True

    movement_time = None

    cv2.namedWindow("extreme-points", cv2.WINDOW_NORMAL)

    while True:
        img = cap.read()[1]
        roi = img[200:900, 468:1440]
        gray_roi = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
        gray_roi_without_valyuz = vr.remove_valyuz(gray_roi)
        malacok = malac_detector.get_malacok(gray_roi_without_valyuz)

        for malac in malacok:
            cv2.circle(roi, malac['head'], 50, (0, 0, 255), 1)
            cv2.circle(roi, malac['butt'], 50, (255, 0, 0), 1)
            # cv2.drawContours(roi, [malac['box']], 0, (0, 0, 255), 2)

        currentBlobs = malacok.copy()
        if len(oldBlobs) == 0:
            oldBlobs = malacok.copy()

        # oldBlobs = checkPosition(oldBlobs, currentBlobs)
        # aggressiveBlobs, friendlyBlobs, eatingBlobs, drinkingBlobs = checkState(aggressiveBlobs, friendlyBlobs,
        #                                                                         eatingBlobs, drinkingBlobs,
        #                                                                         currentBlobs)

        #print(bgsubstract.checkMovement(roi, backSub))
        if bgsubstract.checkMovement(roi, backSub) < 5.0:
            if movementOccurred:
                oldBlobs = checkPosition(oldBlobs, currentBlobs)
                aggressiveBlobs, friendlyBlobs, eatingBlobs, drinkingBlobs = checkState(aggressiveBlobs, friendlyBlobs, eatingBlobs, drinkingBlobs, currentBlobs)
                movementOccurred = False

        else:
            movementOccurred = True

        if movement_time is not None:
            if time.time()-movement_time > 3:
                sendData(5,0)
                movement_time = None

        drawEating(eatingBlobs)
        drawDrinking(drinkingBlobs)
        drawAggressive(aggressiveBlobs)
        drawFriendly(friendlyBlobs)

        cv2.imshow("extreme-points", roi)
        cv2.waitKey(1)
